# Redmine & Sentry docker
## first use    (1. install and update Sentry. 2. install redmine and postgres with initial setting. )
    sudo sh startAllFisrt.sh
## start all
    sudo docker-compose up
## stop all
    sudo docker-compose stop
## destroy all
    sudo sh destroyAll.sh
## how to log in Redmine (port: 8082)
    Account: admin
    Password: admin
## how to log in Sentry (port: 8080)
    set Account and Password by yourself.
