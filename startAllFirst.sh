#!/bin/bash

set -e

echo "remove redmine volume datay..."
rm -rf /srv/docker/redmine/

echo "generate secret key..."
docker run --rm sentry config generate-secret-key

echo "docker-compose up redis postgres sentry..."
docker-compose up -d redis postgres sentry

while [ -z "$(docker logs redis-container 2>&1 | grep 'The server is now ready to accept connections on port 6379')" ]; do
    echo "Waiting redis ready."
    sleep 1
done

while [ -z "$(docker logs postgres-container 2>&1 | grep 'autovacuum launcher started')" ]; do
    echo "Waiting postgres ready."
    sleep 1
done

while [ -z "$(docker logs sentry-container 2>&1 | grep 'ready in')" ]; do
    echo "Waiting sentry ready."
    sleep 1
done

#no create user
echo "do sentry upgrade..."
echo n | docker-compose run sentry sentry upgrade

echo "docker compose up all..."
docker-compose up -d

while [ -z "$(docker logs redmine-pg-container 2>&1 | grep 'autovacuum launcher started')" ]; do
    echo "Waiting redmine postgres ready."
    sleep 1
done

while [ -z "$(docker logs redmine-container 2>&1 | grep 'nginx entered RUNNING state')" ]; do
    echo "Waiting redmine ready."
    sleep 1
done


# init Redmine data.
docker cp ./initDB.sh redmine-pg-container:/initDB.sh
echo "install sql data...."
docker exec -d redmine-pg-container sh /initDB.sh
echo "install sql data....end"

echo "Redmine & Sentry have been successfully installed!!"

